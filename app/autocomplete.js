'use strict';

angular.module('autocomplete', [])
    /**
     * @ngdoc constant
     * @description
     * Contains constants for autocomplete directive
     */
    .constant("AUTOCOMPLETE_CONSTANTS", {
        "DEFAULT_TIMEOUT": 1000,
        "KEY_CODES": {Up: 38, Down: 40},
        "KEYDOWN_LISTENER_NAME": "keydown"
    })
    /**
     * @ngdoc directive
     * @name autocomplete
     *
     * @requires $timeout
     * @requires AUTOCOMPLETE_CONSTANTS
     *
     * @restrict A
     *
     * @description
     * A directive that the search value with results from search-function. Autocompletion starts after the timeout.
     *
     * @example
     * Here's an example of how you'd use autocomplete:
     * <input type="text" autocomplete ng-model="searchAddress" timeout="2000" search-function="getGoogleAddress">
     *
     * @param {string} search - ngModel directive value
     * @param {Function} search-function - function that gets data for autocompletion and return a promise object
     * @param {number} timeout - timeout for autocompletion, 1 sec dy default
     */
    .directive("autocomplete", ["$timeout", "AUTOCOMPLETE_CONSTANTS", function ($timeout, AUTOCOMPLETE_CONSTANTS) {
        return {
            restrict: 'A',

            scope: {
                search: '=ngModel',
                searchFunction: "=",
                timeout: '=?'
            },

            link: function (scope, element, attrs) {
                var timeoutPromise = null;
                var inputElement = element[0];
                var currentOptionIndex = 0;
                var foundOptions = [];

                // True if autocompleted, false if set manually
                var isAutocompleted = false;

                // Set default value for timeout
                if (angular.isUndefined(scope.timeout)) {
                    scope.timeout = AUTOCOMPLETE_CONSTANTS.DEFAULT_TIMEOUT;
                }

                scope.$watch('search', onModelUpdate);

                /**
                 *  Checks the current phase before executing the function.
                 */
                scope.safeApply = function (fn) {
                    var phase = this.$root.$$phase;
                    if (phase == '$apply' || phase == '$digest') {
                        if (fn && (typeof(fn) === 'function')) {
                            fn();
                        }
                    } else {
                        this.$apply(fn);
                    }
                };

                /**
                 * Key down listener for Up and Down arrow controls.
                 */
                var keyDownListener = function (event) {
                    var keyCode = event.keyCode || event.which;
                    switch (keyCode) {
                        case AUTOCOMPLETE_CONSTANTS.KEY_CODES.Up:
                        {
                            onKeyChange(setKeyUpIndex, event);
                            break;
                        }
                        case AUTOCOMPLETE_CONSTANTS.KEY_CODES.Down:
                        {
                            onKeyChange(setKeyDownIndex, event);
                            break;
                        }
                    }
                };
                inputElement.addEventListener(AUTOCOMPLETE_CONSTANTS.KEYDOWN_LISTENER_NAME, keyDownListener);

                scope.$on("$destroy", function (event) {
                    $timeout.cancel(timeoutPromise);
                    inputElement.removeEventListener(AUTOCOMPLETE_CONSTANTS.KEYDOWN_LISTENER_NAME, keyDownListener);
                });

                /**
                 * Listener for ngModel value.
                 * If the value has been changed manually asks data for the value with timeout.
                 */
                function onModelUpdate(newValue, oldValue) {
                    if (scope.searchFunction) {
                        if (!isAutocompleted && newValue) {
                            if (timeoutPromise) {
                                $timeout.cancel(timeoutPromise);
                            }
                            //empty array no to give ability to use arrows
                            foundOptions = [];
                            timeoutPromise = $timeout(getData, scope.timeout);
                        }
                        else {
                            isAutocompleted = false;
                        }
                    }
                }

                /**
                 * Gets the found data by calling the data function attribute.
                 * If the data array isn't empty sets the search value to the first element from the array
                 */
                function getData() {
                    var res = scope.searchFunction(scope.search);
                    res.then(function (data) {
                        foundOptions = data;
                        currentOptionIndex = 0;
                        if (foundOptions && foundOptions[currentOptionIndex]) {
                            setSearchValueFromOptions();
                        }
                        else {
                            console.warn("Options are empty.");
                        }
                    });
                }

                /**
                 * Sets value from found options.
                 */
                function setSearchValueFromOptions() {
                    if (!angular.equals(scope.search, foundOptions[currentOptionIndex])) {
                        scope.search = foundOptions[currentOptionIndex];
                        isAutocompleted = true;
                        scope.safeApply();
                    }
                }

                /**
                 * Handler for keyboard Up and Down arrow controls.
                 * Sets index and value.
                 */
                function onKeyChange(setIndex, event) {
                    if (foundOptions && foundOptions.length != 0) {
                        setIndex();
                        setSearchValueFromOptions();
                        event.preventDefault();
                    }
                }

                /**
                 * Sets index to the previous index of current one or to the last index if current index is 0
                 */
                function setKeyUpIndex() {
                    var lastIndex = foundOptions.length - 1;
                    var nextIndex = currentOptionIndex - 1;
                    currentOptionIndex = nextIndex >= 0 ? nextIndex : lastIndex;
                }

                /**
                 * Sets index to the next index of current one or to 0 if current index is the last one
                 */
                function setKeyDownIndex() {
                    var lastIndex = foundOptions.length - 1;
                    var nextIndex = currentOptionIndex + 1;
                    currentOptionIndex = nextIndex <= lastIndex ? nextIndex : 0;
                }
            }
        }
    }]);

