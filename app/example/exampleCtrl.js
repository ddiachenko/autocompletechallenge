'use strict';

//Controller for the example, gets search data
angular.module('quandooChallenge').controller('exampleCtrl', function ($scope, $http, $q) {
    $scope.searchAddress = "";
    $scope.searchSimpleData = "";
    $scope.names = ['Alex', 'Alla', 'Anton', 'Boris', 'Bred', 'Bein', 'Cat', 'Claris', 'Clara'];

    /**
     * Sends http a request to google search API for getting an array with addresses that contains the viewValue.
     * Creates an array from formatted_address values and resolves the promise with it.
     * @param viewValue {string} - a search value
     * @returns promise object
     */
    $scope.getGoogleAddress = function (viewValue) {
        var deferred = $q.defer();
        var params = {address: viewValue, sensor: false};
        $http.get('http://maps.googleapis.com/maps/api/geocode/json', {params: params})
            .then(function (res) {
                var formattedResults = res.data.results.map(function (item) {
                    return item.formatted_address;
                });
                deferred.resolve(formattedResults);
            });
        return deferred.promise;
    };

    /**
     * Resoles promise with names started from the viewValue
     * @param {string} viewValue - a search value
     * @returns promise object
     */
    $scope.getSimpleData = function (viewValue) {
        var deferred = $q.defer();
        var formattedResults = $scope.names.filter(function (name) {
            var nameLowCase = name.toLowerCase();
            var viewValueLowCase = viewValue.toLowerCase();
            return nameLowCase.indexOf(viewValueLowCase) == 0;
        });
        deferred.resolve(formattedResults);
        return deferred.promise;
    };
});

