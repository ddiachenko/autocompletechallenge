#Task
Implement an AutoCompletion component in AngularJS from scratch. The goal
is to have a single HTML page including a Search Input field which is spiced up with your
AutoCompletion. The Search itself may target any public search API available on the
Internet (e.g. Google, Github, Twitter,...) or simple mock data. A result view is not required,
it's only about the AutoCompletion.

#Description:
The Project contains:
    automomplete module (app/autocomplete.js),
    quandooChallenge application module (app/app.js),
    exampleCtrl that gets data for autocomplete (app/example/exampleCtr.js)
    css for example (app/app.css)

There are two types of data for autocompletion: a simple array with names and data returned with google public search API for geocoded addresses.
The module gives an opportunity to switch autocompletion with keyboard arrow controls.

#Used Libraries:
I didn't need any libraries.

#How to start:
npm start
